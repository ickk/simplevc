from shutil import copyfile
from time import time
from hashlib import sha256
import os

class SimpleVC():
	def __init__(self, working_dir):
		self._wd = working_dir
	def initialise(self):
		"""
		Setup/initialise svc folder structure
		"""
		if not os.path.exists(self._wd+"/.svc"):
			os.makedirs(self._wd+"/.svc/history", exist_ok=True)
			open(self._wd+"/.svc/tracked_files","w").close()
		else:
			raise FileExistsError("simplevc is already instantiated in current working directory: "+self._wd)

	def a_changed(self, file, new_file_bytes):
		latest_hashval = self.get_history(file)["hash"]
		working_hashval = sha256(new_file_bytes).hexdigest()
		return (latest_hashval != working_hashval)

	def is_changed(self, fileset):
		"""
		Compute and compare the sha256 hash of the working file to the hash of the latest stored version of the file.
		Alternatively, if a list or set of files is passed in, returns the subset of
		those files that are changed.
		"""		
		# generalise input
		if not isinstance(fileset, set):
			if isinstance(fileset, str):
				files = {fileset} # convert str to set
			elif isinstance(fileset, list):
				files = set(fileset) # convert list to set
			else: raise TypeError("fileset is not set or list or str")
		else:
			files = fileset
		# generate set of changed files
		result = set()
		for file in files:
			latest_hashval = self.get_history(file)["hash"]
			working_hashval = self.gen_file_sha256(self._wd+"/"+file)
			if latest_hashval != working_hashval:
				result.add(file)
		# unpack result if input is str
		if fileset is str:
			if result: return True
			else: return False
		else:
			return result

	def is_tracked(self, fileset):
		"""
		Returns whether or not (boolean) a file is in the list of tracked files.
		Alternatively, if a list or set of files is passed in, returns the subset of
		those files that are tracked.
		"""
		# generalise input
		if not isinstance(fileset, set):
			if isinstance(fileset, str):
				files = {fileset} # convert str to set
			elif isinstance(fileset, list):
				files = set(fileset) # convert list to set
			else: raise TypeError("fileset is not set or list or str")
		else:
			files = fileset
		# generate set of tracked files
		tracked_files = self.get_tracked(return_type=set)
		#print(tracked_files) # debug
		# find intersection of tracked files and input
		result = set(tracked_files).intersection(files)
		# unpack result if input is str
		if fileset is str:
			if result: return True
			else: return False
		else:
			return result

	def get_tracked(self, return_type=set):
		"""
		Return a list (or return_type=list) of all tracked filenames.
		"""
		with open(self._wd+"/.svc/tracked_files", "r") as fp:
			if return_type == list:
				tracked_files = []
				for line in fp:
					tracked_files.append(line.strip())
			elif return_type == set:
				tracked_files = set()
				for line in fp:
					tracked_files.add(line.strip())
			else:
				raise ValueError("return_type is not set or list")
		return tracked_files

	def get_history(self, filename, entries=None):
		"""
		Return the last version history entry (string) for the given filename.
		If entries (int) is specified, then return (list) of the last few log entries as specified or all (if entries="all").
		"""
		assert self.is_tracked(filename)
		if entries is not None:	
			lines = entries
		else: 
			lines = 1
		history = []
		with open(self._wd+"/.svc/history/"+filename+"/log", "r") as fp:
			if entries == "all": lines = fp
			else: lines = self.tail(fp, lines=lines, return_type="list")
			for line in lines:
				version, hashvalue, time, user, message = line.split(" ", 4)
				history.append({
					"version": version,
					"hash": hashvalue,
					"time": time,
					"user": user,
					"message": message
					})
		if entries is not None:
			return history
		else:
			return history[0]

	def get_versions(self, filename, versions=None):
		"""
		Return (list) of log entries as specified.
		"""
		if isinstance(versions, int): versions = [versions]
		output = []
		for entry in reversed(self.get_history(filename, entries="all")):
			if int(entry["version"]) in versions:
				output.append(entry)
		return output

	def a_add(self, filename, user):
		"""
		Begin tracking file, (commits empty file with target filename).
		"""
		assert not self.is_tracked(filename)
		# instantiate tracking
		dirname = self._wd+"/.svc/history/"+filename
		os.makedirs(os.path.dirname(dirname+"/0"))
		open(dirname+"/0", "w").close()
		nullhash = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" # gen_file_sha256(dirname+"/0")
		with open(dirname+"/log", "w") as fp:
			fp.write("0 "+nullhash+" "+str(int(time()))+" "+user+" Initial commit\n")
		# add to list of tracked files
		with open(self._wd+"/.svc/tracked_files", "a") as fp:
			fp.write(filename+"\n")
		return dirname+"/0"

	def add(self, filename, user="Unspecified"):
		"""
		Begin tracking file, (commits empty file with target filename).
		"""
		#assert not is_tracked(filename)
		# instantiate tracking
		dirname = self._wd+"/.svc/history/"+filename
		os.makedirs(os.path.dirname(dirname+"/0"))
		open(dirname+"/0", "w").close()
		nullhash = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855" # gen_file_sha256(dirname+"/0")
		with open(dirname+"/log", "w") as fp:
			fp.write("0 "+nullhash+" "+str(int(time()))+" "+user+" Initial commit\n")
		# add to list of tracked files
		with open(self._wd+"/.svc/tracked_files", "a") as fp:
			fp.write(filename+"\n")

	def a_commit(self, filename, bytestring, user, message, unchanged_ok=False):
		"""
		Commit new version to history.
		"""
		#                 (last-most hash != new hash)
		bytestring_hash = sha256(bytestring).hexdigest()
		if (self.get_history(filename)["hash"] != bytestring_hash) or unchanged_ok:
			# update version log
			version = str(int(self.get_history(filename)["version"])+1)
			with open(self._wd+"/.svc/history/"+filename+"/log","a") as fp:
				fp.write(version+" "+bytestring_hash+" "+str(int(time()))+" "+user+" "+message+"\n")
			# write file contents to history
			destination = self._wd+"/.svc/history/"+filename+"/"+version
			with open(destination, "wb") as fp:
				fp.write(bytestring)
			return destination
		else:
			print(self.get_history(filename)["hash"] != bytestring_hash)
			print("old: "+self.get_history(filename)["hash"]+" new: "+bytestring_hash)
			raise Exception("file has not changed")
			#TODO define and throw more descriptive exception

	def commit(self, filename, user="Unspecified", message=""):
		"""
		Commit new version of file to history.
		"""
		#assert is_tracked(filename) and is_changed(filename)
		hashvalue = self.gen_file_sha256(self._wd+"/"+filename)
		version = str(int(self.get_history(filename)["version"])+1)
		# update file's version log
		with open(self._wd+"/.svc/history/"+filename+"/log","a") as fp:
			fp.write(version+" "+hashvalue+" "+str(int(time()))+" "+user+" "+message+"\n")
		# copy file contents to history
		source = self._wd+"/"+filename
		destination = self._wd+"/.svc/history/"+filename+"/"+version
		copyfile(source, destination)

	def restore(self, filename, version=None):
		"""
		Restores version of file to the working copy location (overwriting the working copy if it exists), 
		Restores the most recent version unless otherwise specified.
		"""
		if version is None:
			version = self.get_history(filename)["version"]
		source = self._wd+"/.svc/history/"+filename+"/"+str(version)
		destination = self._wd+"/"+filename
		copyfile(source, destination)

	def verify(self, filename, versions="all"):
		"""
		Verify that the hash of the stored versions matches the recorded hash in the version log.
		"""
		if versions == "latest":
			versions = self.get_history(filename, entries=1)
		elif versions == "all":
			versions = self.get_history(filename, entries="all")
		else:
			versions = self.get_versions(filename, versions=versions)
		output = []
		for item in versions:
			h = self.gen_file_sha256(self._wd+"/.svc/history/"+filename+"/"+item["version"])
			if h != item["hash"]:
				output.append((item["version"],"FAIL"))
			else:
				output.append((item["version"],"SUCCESS"))
		return output

	def gen_file_sha256(self, filename, buf_size=65536):
		"""
		Generated the sha256 hash (hexdigest) of a file, using a buffer to conserve mem usage.
		"""
		h = sha256()
		with open(filename, "rb") as fp:
			while True:
				buf = fp.read(buf_size)
				if not buf:
					break
				h.update(buf)
		return h.hexdigest()

	def gen_bytes_sha256(self, bytes_obj):
		"""
		Generate the sha256 hash of a bytestring.
		"""
		return sha256(bytes_obj).hexdigest()

	def tail(self, fp, lines=1, return_type="string"):
		"""
		Return the last lines from a file oblect (default 1).
		Code borrowed from:	https://stackoverflow.com/questions/136168/get-last-n-lines-of-a-file-with-python-similar-to-tail
		"""
		total_lines_wanted = lines
		BLOCK_SIZE = 1024
		fp.seek(0, 2)
		block_end_byte = fp.tell()
		lines_to_go = total_lines_wanted
		block_number = -1
		blocks = [] # blocks of size BLOCK_SIZE, in reverse order starting
					# from the end of the file
		while lines_to_go > 0 and block_end_byte > 0:
			if (block_end_byte - BLOCK_SIZE > 0):
				# read the last block we haven't yet read
				fp.seek(block_number*BLOCK_SIZE, 2)
				blocks.append(fp.read(BLOCK_SIZE))
			else:
				# file too small, start from begining
				fp.seek(0,0)
				# only read what was not read
				blocks.append(fp.read(block_end_byte))
			lines_found = blocks[-1].count('\n')
			lines_to_go -= lines_found
			block_end_byte -= BLOCK_SIZE
			block_number -= 1
		all_read_text = "".join(reversed(blocks))
		if return_type == "list":
			return all_read_text.splitlines()[-total_lines_wanted:]
		elif return_type == "string":
			return "\n".join(all_read_text.splitlines()[-total_lines_wanted:])
		else: 
			raise ValueError("return_type not recognised")

	def get_paths(self, root, exclude):
		"""
		Generates list of relative file paths decending root (using os.walk).
		Excludes files, folders and their chilren in exclude set.
		"""
		if not isinstance(exclude, set):
			exclude = set(exclude)
		paths = set()
		for root, dirs, files in os.walk(root, topdown=True):
			dirs[:] = [d for d in dirs if d not in exclude]
			for file in files:
				file = os.path.join(os.path.relpath(root, self._wd), file)
				if file[0:2] == "./":
					paths.add(file[2:])
				else:
					paths.add(file)
		return paths-exclude