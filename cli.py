#!/usr/bin/env python3
from sys import argv
from datetime import datetime
from getpass import getuser
import os
import argparse

from simplevc import SimpleVC

def get_user(args):
	"""
	Fetches username from command line, 
	else tries from .svc/user
	else from env variable "SIMPLEVC_USER"
	else from user session
	
	Returns username
	"""
	if args.user is not None:
		return args.user
	elif os.path.isfile(working_dir+"/.svc/user"):
		with open(working_dir+"/.svc/user", "r") as fp:
			return fp.readline().strip()
	elif "SIMPLEVC_USER" in os.environ:
		return os.environ["SIMPLEVC_USER"]
	else:
		return getuser()

def parse_init(args):
	"""
	Setup/initialise simplevc folder structure
	"""
	svc = SimpleVC(working_dir)

	try:
		svc.initialise()
	except FileExistsError:
		print("simplevc appears to already be instantiated in current working directory: "+working_dir)

def parse_add(args):
	"""
	Begins tracking files specified.
	"""
	svc = SimpleVC(working_dir)

	files = set(args.file)
	tracked = svc.is_tracked(files)
	not_tracked = files - tracked
	for file in not_tracked:
		svc.add(file, user=get_user(args))
	if tracked:
		print("The following files are already being tracked:\n  ", end="")
		for file in tracked:
			print(file, end=" ")
		print()

def parse_commit(args):
	"""
	Commits changes to files specified.
	"""
	svc = SimpleVC(working_dir)

	files = set(args.file)
	tracked = svc.is_tracked(files)
	changed = svc.is_changed(tracked)
	for file in changed:
		svc.commit(file, user=get_user(args), message=args.message)
	not_changed = tracked - changed
	not_tracked = files - tracked
	if not_changed:
		print("Files not committed because they have not changed:\n  ", end="")
		for file in not_changed:
			print(file, end=" ")
		print() # newline
	if not_tracked:
		print("Files not committed because they are not being tracked (add them first):\n  ", end="")
		for file in not_tracked:
			print(file, end=" ")
		print()

def parse_restore(args):
	"""
	Reverts file to previous version.
	"""
	svc = SimpleVC(working_dir)

	version = args.version
	latest = int(svc.get_history(args.file)["version"])
	if version < latest and version > 0:
		svc.restore(args.file, version)
	else:
		print()

def parse_verify(args):
	"""
	Prints results of checks between stored files and hashes in log.
	"""
	svc = SimpleVC(working_dir)

	if hasattr(args, 'file'):
		if args.version:
			results = svc.verify(args.file, args.version)
		else:
			results = svc.verify(args.file)
		for item in results:
			print(" ", item[0], item[1])

def parse_verify_all(args):
	"""
	Prints result of checks betweened all stored files and their logs.
	"""
	svc = SimpleVC(working_dir)

	files = svc.get_tracked(return_type=list)
	for file in files:
		results = svc.verify(file, versions="all")
		print(file)
		for item in results:
			print(" ", item[0], item[1])

def parse_log(args):
	"""
	Print log entries for file. 
	Print `count`(int) number of entries (default 5).
	If version number given, print log entries surrounding that version (count 
	default 1),	otherwise print latest entries.
	"""
	svc = SimpleVC(working_dir)

	if args.all:
		latest = int(svc.get_history(args.file)["version"])
		history = svc.get_history(args.file, entries=latest+1)
	else:
		if args.version is not None:
			# center around version number
			if args.count is not None: 
				entries = args.count
			else: 
				entries = 1
			latest = int(svc.get_history(args.file)["version"])
			history = svc.get_history(args.file, entries=(latest-args.version+entries//2+1))[:entries]
		else:
			# just grab latest entries
			if args.count is not None: 
				entries = args.count
			else: 
				entries = 5
			history = svc.get_history(args.file, entries=entries)
	# print out
	for item in history:
		timestamp = datetime.fromtimestamp(int(item["time"]))
		print("\ncommit", item["version"], item["hash"])
		print("    Author:", item["user"])
		print("    Date:", timestamp)
		print("       ", item["message"])
	print()

def parse_status(args):
	"""
	Print lists of changed files and untracked files.
	"""
	svc = SimpleVC(working_dir)

	# get excludes
	exclude = {".svc","svc.py"}
	try:
		with open(working_dir+"/.svcignore") as fp:
			[exclude.add(line.strip()) for line in fp]
	except FileNotFoundError:
		pass
	# get paths
	paths = svc.get_paths(working_dir, exclude)
	tracked = svc.is_tracked(paths)
	changed = svc.is_changed(tracked)
	not_tracked = paths-svc.is_tracked(paths)
	if changed:
		print("Changes to be committed:\n  (use \""+os.path.basename(argv[0])+" commit <file>...\")\n\033[31m")
		for file in sorted(changed):
			print("       ", file)
		print("\033[0m")
	if not_tracked:
		print("Untracked files:\n  (use \""+os.path.basename(argv[0])+" add <file>...\" to begin tracking)\n\033[31m")
		for file in sorted(not_tracked):
			print("       ", file)
		print("\033[0m")
	elif not changed:
		print("nothing to commit, working directory clean")

def parse_diff(args):
	"""
	Print the diff between two versions of a file.
	"""
	svc = SimpleVC(working_dir)

	usage = "usage: "+os.path.basename(argv[0])+" diff [-h] file [-v [VERSION [VERSION]]]"
	if not svc.is_tracked(args.file):
		print(usage)
		print(os.path.basename(argv[0]), "diff: error: no history available for "+args.file)
		return
	latest_version = int(svc.get_history(args.file)["version"])
	if args.version is not None:
		# note: need to manually update this check and msg
		if len(args.version) > 2:
			print(usage)
			print(os.path.basename(argv[0]), "diff: error: argument -v/--version: expected 1 or 2 arguments")
			return
		else:
			invalid_msg = os.path.basename(argv[0])+" diff: error: argument -v/--version: invalid version: latest version of "+args.file+" is "+str(latest_version)
			if len(args.version) == 1:
				# use working copy and specified version
				if args.version[0] > latest_version:
					print(usage)
					print(invalid_msg)
					return
				version_a = working_dir+"/.svc/history/"+args.file+"/"+str(args.version[0])
				version_b = working_dir+"/"+args.file
			else: # len(args.version) == 2
				# use specified versions
				if args.version[0] > latest_version or args.version[1] > latest_version:
					print(usage)
					print(invalid_msg)
					return
				version_a = working_dir+"/.svc/history/"+args.file+"/"+str(args.version[0])
				version_b = working_dir+"/.svc/history/"+args.file+"/"+str(args.version[1])
	else:
		# use working copy and latest version
		version_a = working_dir+"/.svc/history/"+args.file+"/"+str(latest_version)
		version_b = working_dir+"/"+args.file

	import difflib
	with open(version_a, "r") as fa, open(version_b, "r") as fb:
		diff = difflib.ndiff(fa.readlines(),fb.readlines())
	delta = "".join("\033[92m"+x+"\033[0m" if x.startswith("+ ") else "\033[91m"+x+"\033[0m" if x.startswith("- ") else x for x in diff)
	print(delta)

def main():
	global working_dir 
	working_dir = os.getcwd()

	parser = argparse.ArgumentParser()
	parser.add_argument("-u", "--user")

	subparsers = parser.add_subparsers()
	# status subcommand
	parser_status = subparsers.add_parser("status", help="List changed and untracked files")
	parser_status.set_defaults(func=parse_status)
	# log subcommand
	parser_log = subparsers.add_parser("log", help="Print log entries for file")
	parser_log.add_argument("file")
	parser_log.add_argument("-c", "--count", type=int)
	parser_log.add_argument("-v", "--version", type=int)
	parser_log.add_argument("-a", "--all", action="store_true")
	parser_log.set_defaults(func=parse_log)
	# init subcommand
	parser_init = subparsers.add_parser("init", help="Setup/initialise simplevc folder structure")
	parser_init.set_defaults(func=parse_init)
	# add subcommand
	parser_add = subparsers.add_parser("add", help="Begins tracking files specified")
	parser_add.add_argument("file", nargs="+")
	parser_add.set_defaults(func=parse_add)
	# commit subcommand
	parser_commit = subparsers.add_parser("commit", help="Commits changes to files specified")
	parser_commit.add_argument("file", nargs="+")
	parser_commit.add_argument("-m", "--message", required=True)
	parser_commit.set_defaults(func=parse_commit)
	# restore subcommand
	parser_restore = subparsers.add_parser("restore", help="Reverts file to previous version")
	parser_restore.add_argument("file")
	parser_restore.add_argument("-v", "--version", type=int, required=True)
	parser_restore.set_defaults(func=parse_restore)
	# diff subcommand - note: need to manually update usage message here and in func call
	parser_diff = subparsers.add_parser("diff", help="Print the diff between two versions\
		of a file", usage="%(prog)s [-h] file [-v [VERSION [VERSION]]]")
	parser_diff.add_argument("file")
	parser_diff.add_argument("-v", "--version", nargs="+", type=int)
	parser_diff.set_defaults(func=parse_diff)
	# verify subcommand
	parser_verify = subparsers.add_parser("verify", help="Compares stored files against log hashes")
	parser_verify.add_argument("file")
	parser_verify.add_argument("-v", "--version", nargs="+", type=int)
	parser_verify.set_defaults(func=parse_verify)
	# verify-all subcommand
	parser_verify_all = subparsers.add_parser("verify-all", help="Verifies complete history for all tracked file")
	parser_verify_all.set_defaults(func=parse_verify_all)

	args = parser.parse_args()
	if hasattr(args, "func"):
		args.func(args)
	else:
		parser.print_help()

if __name__ == "__main__":
	main()