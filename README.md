This was merely a learning experience; I don't recommend that this software be used to store anything important.

Project Details
===============
Project structure:
- simplevc.py

  a library that can be included in other projects.
  
- cli.py

  A nice little command line interface that wraps the simplevc library.

Goals:
- Learn about version control systems.
- Implement a simple vcs suited to storing versions of linear potentially unrelated documents (i.e a wiki where individual files need be given "checkpoints", as opposed to a software project where all files need to be committed together).
- Implemented as a library so that it can easily be embedded inside another application.
- Include a command line wrapper.

Features:
- Version control:
  - Acretion of new versions.
  - restoration of older version.
  - Log comments.
- CLI that:
  - somewhat resembles git (although the behaviour of some commands differs to git).
  - has a nice help message.
  - can Produce Diffs.
  - prints change logs nicely.
  - supports a crude implementation of a ".ignore" file (as seen in git).

Known problems:
- max version count(?).
- bug in CLI when commiting multiple files, folders.
- the log mechanism is space and new-line delimmited, consider using null or something so that newlines in fields do not cause problems.

Todo:
- (CLI) Add lots of nice messages when the user tries to do something they can't.
- Write tests or otherwise prove correctness.
- Evaluate performance and usage compared to other version control systems.
- Port to Rust?

CLI usage and examples
======================
The cli for simplevc is reasonably rich.

![](screenshots/cli_help.png)

Starting a new repo and specifying a file to begin tracking looks like the following:

![](screenshots/cli_add.png)

Committing new changes to that file:

![](screenshots/cli_commit.png)

Viewing a log of the most recent changes for a given file:

![](screenshots/cli_log.png)

You can even see the diff between two versions of a file (or, by default the current un-committed file and the most recent version):

![](screenshots/cli_diff.png)

Explanation
===========
If some files exist that needed to be version controled;
```
  about.md
  projects/pegvcs.md
  file_2.md
  file_3.md
```
internally simplevc simple handles the versioning of the files like so;
```
  .svc/
    |-user (optional)
    |-tracked_files
    `-history/
      |-about.md/
      | |-log
      | |-0
      | |-1
      | |-2
      | : :
      | `-F
      |-projects/simplevc.md/
      | |-log
      | |-0
      | `-1
      |-file_2.md/
      | : : : : :
      `-file_3.md/
```
`user` is an optional file containing the name to use in a commit if it is otherwise not specified.

`tracked_files` is a plaintext file that has a record of all of the currently tracked files, and `history` is a directory containing the version information and the different versions of each tracked file.

`log` is a plaintext file associated with each tracked file that stores information about any given version. Currently each entry is newline delimited, with each field being space delimited; the version number, file hash, time (epoch), user, and commit message.

example `log`:
```
  0 e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855 1510400994 Nickk Initial commit
  1 hashhashhashhashhashhashhashhashhashhashhashhashhashhashhashhash 1510404104 Nickk fixed typo
  2 hashhashhashhashhashhashhashhashhashhashhashhashhashhashhashhash 1510429829 Nickk Changed title
```